import logo from './logo.svg';
import './App.css';
import "bootstrap/dist/css/bootstrap.min.css"
import anh1 from './assets/images/1.jpg'
import anh2 from './assets/images/2.jpg'
import anh3 from './assets/images/3.jpg'
import anh4 from './assets/images/4.jpg'
import hawaiian from './assets/images/hawaiian.jpg'
import bacon from './assets/images/bacon.jpg'
import seafood from './assets/images/seafood.jpg'

function App() {
  return (
    <div className="App">
      <div className="container-fluid bg-warning">
        <nav className="navbar navbar-expand bg-warning">
          <div className="col-sm-1">
          </div>
          <div className="col-sm-3">
            <a className="navbar-brand" href="#">Trang chủ</a>
          </div>
          <div className="col-sm-3">
            <a className="navbar-brand" href="#">Combo</a>
          </div>
          <div className="col-sm-3">
            <a className="navbar-brand" href="#">Loai Pizza</a>
          </div>
          <div className="col-sm-2">
            <a className="navbar-brand" href="#">gửi đơn hàng</a>
          </div>
        </nav>
      </div>
      <div>
        <h1 className="text-center text-warning">Pizza 365</h1>
        <h4 className="text-center text-danger"><i>Truly italian!</i></h4>
      </div>

      <div className='container'>
        <div className="col-sm-12">
          <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
              <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
            </ol>
            <div className="carousel-inner">
              <div className="carousel-item active">
                <img className="d-block w-100" src={anh1} alt="First slide" />
              </div>
              <div className="carousel-item">
                <img className="d-block w-100" src={anh2} alt="Second slide" />
              </div>
              <div className="carousel-item">
                <img className="d-block w-100" src={anh3} alt="Third slide" />
              </div>
              <div className="carousel-item">
                <img className="d-block w-100" src={anh4} alt="four slide" />
              </div>
            </div>
            <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
              <span className="carousel-control-prev-icon" aria-hidden="true"></span>
              <span className="sr-only">Previous</span>
            </a>
            <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
              <span className="carousel-control-next-icon" aria-hidden="true"></span>
              <span className="sr-only">Next</span>
            </a>
          </div>
        </div>
      </div>
      {/* tại soa lại Pizaa 365 */}
      <div className="text-center p-4 mt-4">
        <h2 className="text-warning">Tại sao lại Pizza 365</h2>
      </div>
      {/* conten */}
      <div className="col-sm-12">
        <div className="row">
          <div className="col-sm-3 p-4 bg-info text-danger">
            <h2 className="text-center">Đa dạng</h2>
            <p className="text-center ">số lượng Pizza đa dạng, có đầy đủ các loại Pizza đang hot nhất hiện nay</p>
          </div>
          <div className="col-sm-3 p-4 bg-primary text-warning text-center">
            <h2>Chất lượng</h2>
            <p>nguyên liệu sạch sẽ 100% rõ nguồn gốc, quy trình chế biến bảo đãm vệ sinh an toàn thực phẩm</p>
          </div>
          <div className="col-sm-3 p-4 bg-success text-center text-danger">
            <h2>Hương vị</h2>
            <p>Đảm bảo hương vị ngon độc,lạ mà bạn chỉ có thể trải nghiệm từ Pizza 365</p>
          </div>
          <div className="col-sm-3 p-4 bg-warning text-center text-dark">
            <h2>dịch vụ</h2>
            <p>Nhân viên thân thiện,nhà hàng hiện đại, dịch vụ giao hàng nhanh chất lượng, tân tiến</p>
          </div>
        </div>
      </div>

      <div class="text-center p-4 mt-4 text-danger">
        <h2><b class="border-bottom"> Chọn Loại Pizza</b></h2>
      </div>
      {/* card */}
      <div class="col-sm-12">
        <div class="row">
          <div class="col-sm-4">
            <div class="card">
              <div class="card-header bg-warning text-center">
                <h3>S (small)</h3>
              </div>
              <div class="card-body text-center">
                <ul class="list-group list-group-flush">
                  <li class="list-group-item"><b>Đường kính: </b> 20cm</li>
                  <li class="list-group-item"><b>Sường nướng: </b> 2</li>
                  <li class="list-group-item"><b>Salad:</b> 200g</li>
                  <li class="list-group-item"><b>Nước ngọt: </b> 2</li>
                  <li class="list-group-item">
                    <h1><b>150.000</b></h1>
                    <p><b>VND</b></p>
                  </li>
                </ul>
              </div>
              <div class="card-footer bg-warning text-center">
                <button class="form-control btn btn-primary" id="btn-S-chon">Chọn</button>
              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="card">
              <div class="card-header bg-warning text-center">
                <h3>M (Medium)</h3>
              </div>
              <div class="card-body text-center">
                <ul class="list-group list-group-flush">
                  <li class="list-group-item"><b>Đường kính:</b> 25cm</li>
                  <li class="list-group-item"><b>Sường nướng: </b> 4</li>
                  <li class="list-group-item"><b>Salad:</b> 300g</li>
                  <li class="list-group-item"><b>Nước ngọt: </b> 3</li>
                  <li class="list-group-item">
                    <h1><b>200.000</b></h1>
                    <p><b>VND</b></p>
                  </li>
                </ul>
              </div>
              <div class="card-footer bg-warning text-center">
                <button class="form-control btn btn-primary" id="btn-M-chon">Chọn</button>
              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="card">
              <div class="card-header bg-warning text-center">
                <h3>L (Large)</h3>
              </div>
              <div class="card-body text-center">
                <ul class="list-group list-group-flush">
                  <li class="list-group-item"><b>Đường kính:</b> 30cm</li>
                  <li class="list-group-item"><b>Sường nướng: </b> 8</li>
                  <li class="list-group-item"><b>Salad:</b> 500g</li>
                  <li class="list-group-item"><b>Nước ngọt: </b> 4</li>
                  <li class="list-group-item">
                    <h1><b>250.000</b></h1>
                    <p><b>VND</b></p>
                  </li>
                </ul>
              </div>
              <div class="card-footer bg-warning text-center">
                <button class="form-control btn btn-primary" id="btn-L-chon">Chọn</button>
              </div>
            </div>
          </div>

        </div>
      </div>
      {/* __tieu de___ */}
      <div class="text-center p-4 mt-4 text-danger">
        <h2><b class="border-bottom">Chọn Loại Pizza</b></h2>
      </div>

      {/* conten 1 */}
      <div class="col-sm-12">
        <div class="row">
          {/* conten 1 */}
          <div class="col-sm-4">
            <div className="card w-100" style={{ width: '18rem' }}>
              <img src={seafood} class="card-img-top" alt='seafood' />
              <div className="card-body text-center">
                <h3 class="text-danger"><b>OCEAN MANIA</b></h3>
                <p class="text-info">PIZZA HẢI SẢN SỐT MAYONNAISE</p>
                <p>Xốt cà chua, phô mai mozzarealla, tôm, mực, thanh cua, hành tây.</p>
              </div>
              <div class="card-footer text-center">
                <button class="btn btn-warning form-control" id="pizza-ocean-mania">Chọn</button>
              </div>
            </div>
          </div>
          {/* conten 2 */}
          <div class="col-sm-4">
            <div class="card w-100" style={{ width: '18rem' }}>
              <img src={hawaiian} class="card-img-top" alt='hawaiian' />
              <div class="card-body text-center">
                <h3 class="text-danger"><b>HAWAIIAN</b></h3>
                <p class="text-info">PIZZA DĂM BÔNG DỨA KIỂU HAWAI</p>
                <p>Xốt cà chua, phô mai mozzarealla, tôm, mực, thị dăm bông, thơm.</p>
              </div>
              <div class="card-footer text-center">
                <button class="btn btn-warning form-control" id="pizza-hawai">Chọn</button>
              </div>
            </div>
          </div>
          {/* conten 2 */}
          <div class="col-sm-4">
            <div class="card w-100" style={{ width: '18rem' }}>
              <img src={bacon} class="card-img-top" alt="bacon" />
              <div class="card-body text-center">
                <h3 class="text-danger"><b>BACON</b></h3>
                <p class="text-info">PIZZA PHÔ MAI THỊT HEO XÔNG KHÓI </p>
                <p>Xốt phô mai mozzarealla, thịt Heo muối, cà chua</p>
              </div>
              <div class="card-footer text-center">
                <button class="btn btn-warning form-control" id="pizza-thit-hun-khoi">Chọn</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* ___het con ten____ */}

      {/* ---chon do uong---- */}
      <div class="text-center p-4 mt-4 text-warning">
        <h2><b class="border-bottom">Chọn đồ uông</b></h2>
      </div>
      <div class="col-sm-12">
        <select id="select-do-uong" class=" form-control">
          <option value="0">chọn loại dồ uống</option>
        </select>
      </div>
      {/* -----het chon do uong---- */}

      {/* ------gui don hang------- */}
      <div class="text-center p-4 mt-4 text-danger">
        <h2><b class="border-bottom">Gửi đơn hàng</b></h2>
      </div>
      {/* ---het gui don hang---- */}
      {/* <!-- hiên thị đơn --> */}
      <div class="col-sm-12">
        <div class="row">
          <div class="col-sm-12">
            <div class="form-group">
              <label for="fullname">Họ tên</label>
              <input
                type="text"
                class="form-control"
                id="inp-fullname"
                placeholder="nhập tên"
              />
              <div class="form-group">
                <label for="dien-thoai">Email</label>
                <input
                  type="text"
                  class="form-control"
                  id="inp-email"
                  placeholder="nhập số điện thoại"
                />
              </div>
            </div>
            <div class="form-group">
              <label for="dien-thoai">Số điện thoại</label>
              <input
                type="text"
                class="form-control"
                id="inp-phone-number"
                placeholder="nhập số điện thoại"
              />
            </div>
            <div class="form-group">
              <label for="dia-chi">địa chỉ</label>
              <input
                type="text"
                class="form-control"
                id="inp-address"
                placeholder="nhập địa chỉ"
              />
            </div>
            <div class="form-group">
              <label for="message">Mã giảm giá</label>
              <input
                type="text"
                class="form-control"
                id="inp-vocuher"
                placeholder="Mã Giảm giá"
              />
            </div>
            <div class="form-group">
              <label for="message">Lời nhắn</label>
              <input
                type="text"
                class="form-control"
                id="inp-loinhan"
                placeholder="lời nhắn"
              />
            </div>
            <button type="button" class="btn btn-primary w-100 font-weight-bold" id="btn-gui-don">Gủi</button>
          </div>
        </div>
      </div>

      {/* <!-- lời cảm ơn --> */}
      <div class="container bg-warning p-4 mt-4 loichao" style={{ display: "none" }}>
        <h3 class="text-center">cảm ơn bạn dã đặt hàng tại Pizza 365, Mã đơn hàng của bạn là</h3>
        <div class="row">
          <div class="col-sm-4">
            <h3>mã đơn hang =</h3>
          </div>
          <div class="col-sm-8">
            <input type="text" id="inp-idorder" />
          </div>
        </div>
      </div>
      {/* <!-- modal đơn hang --> */}
      <div>
        <div id="update-user-modal" class="modal fade" tabindex="-1">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="h5-modal-title">Thông tin đơn hàng</h5>
                <button class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body">
                <form>
                  <div class="container">
                    <div class="row form-group">
                      <label>Họ tên</label>
                      <input id="input-hoten" class="form-control" />
                    </div>
                    <div class="row form-group">
                      <label>Số điện thoại</label>
                      <input id="input-SĐT" class="form-control" />
                    </div>
                    <div class="row form-group">
                      <label>Địa chỉ</label>
                      <input id="input-diachi" class="form-control" />
                    </div>
                    <div class="row form-group">
                      <label>Lời nhắn</label>
                      <input id="input-loinhan" class="form-control" />
                    </div>
                    <div class="row form-group">
                      <label>Mã giảm giá</label>
                      <input id="input-MGG" class="form-control" />
                    </div>
                    <div class="row form-group">
                      <label>Thông tin chi tiết</label>
                      <textarea id="input-TTCT" class="form-control"></textarea>
                    </div>
                  </div>
                </form>
              </div>
              <div class="modal-footer">
                <button class="btn btn-primary" id="btn-tao-don" >Tạo đơn</button>
                <button class="btn btn-danger" id="btn-Cancel" data-dismiss="modal">Quay lại</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* <!-- Footer --> */}
      <div class="container-fluid bg-warning p-4 mt-4">
        <div class="row text-center">
          <div class="col-sm-12">
            <h4 class="m-2">Footer</h4>
            <a href="#" class="btn btn-dark m-3"><i class="fa fa-arrow-up"></i>To the top</a>
            <div class="m-2">
              <i class="fa fa-facebook-official w3-hover-opacity"></i>
              <i class="fa fa-instagram w3-hover-opacity"></i>
              <i class="fa fa-snapchat w3-hover-opacity"></i>
              <i class="fa fa-pinterest-p w3-hover-opacity"></i>
              <i class="fa fa-twitter w3-hover-opacity"></i>
              <i class="fa fa-linkedin w3-hover-opacity"></i>
            </div>
          </div>
        </div>
      </div>
    {/* het footer */}

    </div>
  );
}

export default App;
